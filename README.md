# magic-web

https://magic-web-9925c.firebaseapp.com

## Update navigation header (top bar)
1. goto file src/app/routs.ts
inside the file please change the object navLinks 
by adding new lines as required.
please make sure not to break the structure of the object.

in case you add a new item to the navLinks object you need to add an additional line to the 
routes variable in the same file (routes.ts)
 
 for example:
 
 Current state:
 
 export const  navLinks = [
   { name: 'GUIDES'   , url: 'guides' },
   { name: 'DOCS'     , url: 'docs' },
   { name: 'RESOURCES', url: 'resources'},
   { name: 'EVENTS'   , url: 'events'}
 ];

and the following code as well inside the 
export const routes: Routes = [
  ....

  {
    path: 'events',
    component: MgEventsComponent
  },
  ...
]
  
  and i need to add a new menu items with the name "API"
  
  then the code should look like:
  
  export const  navLinks = [
     { name: 'GUIDES'   , url: 'guides' },
     { name: 'DOCS'     , url: 'docs' },
     { name: 'RESOURCES', url: 'resources'},
     { name: 'EVENTS'   , url: 'events'},
     { name: 'API'   , url: 'api'}
   ];

and to the new path as follows:

export const routes: Routes = [
  ....

  {
    path: 'events',
    component: MgEventsComponent
  },
  {
      path: 'api',
      component: MgAPIComponent
    },
  ...
]

 additionally you will nee dto create the MgAPIComponent and have the content written there.
 
 ## adding a tree item to Docs
 assume we would like to add a new item by the name "Demo1"
 go to file app/content/docs/pages and create a new component called Demo1PageComponent class ( file demo1-page.component.ts ) 
 (note the format and the lowercase )
 
 register the component in the NgModule (docs.module.ts) under COMPONENTS array.
 
 in the file app/content/docs/routes.ts please add the following:
 
export const routes: Routes = [
  {
    path: '',
    component: MgDocsComponent,
    children: [
      { // Top level
        path : 'introduction',
        //component : MgDocPageComponent,
        children : [
           // second level
          { path: 'apps powered by angular' , component : AppsPoweredByAngularComponent},
          { path: 'the skill set' , component : TheSkillSetComponent},
          { path: 'why magic web?' , component : WhyMagicWebComponent},
          
          { path: 'my demo 1' , component : Demo1PageComponent},

        ]
      },
    ]
  }
]
