import { Routes } from '@angular/router';
import { MgNotFoundPageComponent } from './modules/core/containers/notFound/notFoundPage.component';
import { MgWelcomeComponent } from './modules/core/containers/welcome/welcome.component';
import { MgResourcesComponent } from './contents/resources/resources.component';
import { MgEventsComponent } from './contents/events/events.component';


export const  navLinks = [
  { name: 'GUIDES'   , url: 'guides' },
  { name: 'DOCS'     , url: 'docs' },
  { name: 'RESOURCES', url: 'resources'},
  { name: 'EVENTS'   , url: 'events'}
];

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: MgWelcomeComponent
  },
  {
    path: 'guides',
    loadChildren: './contents/guides/guides.module#GuidesModule'
  },
  {
    path: 'docs',
    loadChildren: './contents/docs/docs.module#DocsModule'
  },
  {
    path: 'resources',
    component: MgResourcesComponent
  },
  {
    path: 'events',
    component: MgEventsComponent
  },
  { path: '**',
    component: MgNotFoundPageComponent
  }
];
