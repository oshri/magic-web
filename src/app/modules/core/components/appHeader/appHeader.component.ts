import { Inject, Component , OnInit, HostListener, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { SideNavControl } from '../../services/sideNavControl/sideNavControl.service';
import { MgSearchComponent } from '../../../shared/components/search/search.component';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { BehaviorSubject, Observable } from 'rxjs';
import {navLinks} from '../../../../routes';

@Component({
  selector: 'mg-app-header',
  styleUrls: ['appHeader.component.scss'],
  templateUrl: 'appHeader.component.html',
  animations: [
    trigger('menuButton', [
      state('true', style({ opacity: 1, display: 'block', transform: 'translateX(0px)' })),
      state('false', style({ opacity: 0, display: 'none', transform: 'translateX(-100px)' })),
      transition('false <=> true', animate(500))
    ]),
    trigger('navWrapper', [
      state('true', style({ transform: 'translateX(0px)' })),
      state('false', style({ transform: 'translateX(100px)' })),
      transition('false <=> true', animate(500))
    ])
  ]
})
export class MgAppHeaderComponent implements OnInit {
  showMenu$: BehaviorSubject<boolean>;

  @ViewChild(MgSearchComponent)
  searchBox: MgSearchComponent;

  searchResults: Observable<any>;
  showSearchResults = false;

  /*navLinks = [
    { name: 'GUIDES'   , url: 'guides' },
    { name: 'DOCS'     , url: 'docs' },
    { name: 'RESOURCES', url: 'resources'},
    { name: 'EVENTS'   , url: 'events'}
  ];*/

  get navLinks (){
    return navLinks;
  }

  constructor(
    @Inject(Router) private router: Router,
    @Inject(SideNavControl) public sideNavControl: SideNavControl
  ) {}

  ngOnInit() {

  }

  toggleSideMenu() {
    this.sideNavControl.dataDrawer.toggle();
  }

  /**
   * Search
   */

  hideSearchResults() {
    this.showSearchResults = false;
  }

  focusSearchBox() {
    if (this.searchBox) {
      this.searchBox.focus();
    }
  }

  doSearch(query: string) {
    // this.searchResults = this.searchService.search(query);
    this.showSearchResults = !!query;
  }

  @HostListener('document:keyup', ['$event.key', '$event.which'])
  onKeyUp(key: string, keyCode: number) {
    // forward slash "/"
    if (key === '/' || keyCode === 191) {
      this.focusSearchBox();
    }
    if (key === 'Escape' || keyCode === 27 ) {
      // escape key
      if (this.showSearchResults) {
        this.hideSearchResults();
        this.focusSearchBox();
      }
    }
  }
}
