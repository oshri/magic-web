import { Component, Input } from '@angular/core';


@Component({
  selector: 'mg-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: 'footer.component.html'
})
export class MgFooterComponent {
  @Input() nodes: any;
}
