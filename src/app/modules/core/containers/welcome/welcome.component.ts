import { Component } from '@angular/core';

@Component({
  selector: 'mg-welcome',
  styleUrls: ['./welcome.component.scss'],
  templateUrl: './welcome.component.html'
})
export class MgWelcomeComponent {}
