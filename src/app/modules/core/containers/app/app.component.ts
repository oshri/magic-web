import { Component } from '@angular/core';
import { fadeAnimation } from '../../../../animations';

@Component({
  selector: 'mg-app',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
  animations: [fadeAnimation]
})
export class MgAppComponent {

}
