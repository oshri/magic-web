import { Component } from '@angular/core';

@Component({
  selector: 'mg-not-found-page',
  styleUrls: ['./notFoundPage.component.scss'],
  templateUrl: './notFoundPage.component.html'
})
export class MgNotFoundPageComponent {}
