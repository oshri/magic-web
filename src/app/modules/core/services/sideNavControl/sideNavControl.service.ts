import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class SideNavControl {
  drawerFn$ = new BehaviorSubject<any>(null);

  get dataDrawer(): any {
    return this.drawerFn$.value;
  }

  attachDrawer(fn: any) {
    this.drawerFn$.next(fn);
  }

  detachDrawer() {
    this.drawerFn$.next(null);
  }

}
