import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material';

// Magic Modules
import { MgSharedModule } from '../shared';

// Containers
import { MgAppComponent } from './containers/app/app.component';
import { MgNotFoundPageComponent } from './containers/notFound/notFoundPage.component';
import { MgResourcesComponent } from '../../contents/resources/resources.component';
import { MgEventsComponent } from '../../contents/events/events.component';

// Components
import { MgLayoutComponent } from './components/layout/layout.component';
import { MgAppHeaderComponent } from './components/appHeader/appHeader.component';
import { MgFooterComponent } from './components/footer/footer.component';
import { MgWelcomeComponent } from './containers/welcome/welcome.component';

// Services
import { SideNavControl } from './services/sideNavControl/sideNavControl.service';


export const IMPORT = [
  CommonModule,
  RouterModule,
  MaterialModule,
  MgSharedModule,
  BrowserAnimationsModule,
];

export const COMPONENTS = [
  MgAppComponent,
  MgNotFoundPageComponent,
  MgLayoutComponent,
  MgAppHeaderComponent,
  MgFooterComponent,
  MgWelcomeComponent,
  MgResourcesComponent,
  MgEventsComponent
];

@NgModule({
  imports: IMPORT,
  declarations: COMPONENTS,
  exports: COMPONENTS,
  entryComponents: []
})
export class MgCoreModule {
  static forRoot() {
    return {
      ngModule: MgCoreModule,
      providers: [ SideNavControl ]
    };
  }
}
