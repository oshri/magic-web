import { Injectable } from '@angular/core';
import { Route } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { IFileNode } from '../../models';


@Injectable()
export class FileDatabase {
  dataChange = new BehaviorSubject<IFileNode[]>([]);

  get data(): IFileNode[] { return this.dataChange.value; }

  constructor() { }

  initialize(routes: Route) {
    const data = this.buildTreeFromRouter(routes, 0);

    this.dataChange.next(data);
  }

  buildTreeFromRouter(obj: object, level: number, parent?: any): IFileNode[] {
    return Object.keys(obj).reduce<IFileNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new IFileNode();
      node.filename = key;

      if (typeof parent !== 'undefined') {
        node.url = `${parent.url ? parent.url : parent.filename}/${node.filename}`;
      }

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildTreeFromRouter(value, level + 1, node);
        } else {
          node.type = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

}
