import { Component, Input, OnInit, Inject, ViewChild , ElementRef, AfterViewInit, OnDestroy} from '@angular/core';
import { IFileNode } from '../../models';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Routes, Route } from '@angular/router';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Observable, of as observableOf } from 'rxjs';
import { FileDatabase } from '../../services/fileFlatNode/fileFlatNode.service';
import { SideNavControl } from '../../../core/services/sideNavControl/sideNavControl.service';
/** Flat node with expandable and level information */
export class FileFlatNode {
  constructor(
    public expandable: boolean,
    public filename: string,
    public level: number,
    public type: any,
    public url?: string
  ) {}
}

@Component({
  selector: 'mg-side-nav-router',
  styleUrls: ['./sideNavRouter.component.scss'],
  templateUrl: './sideNavRouter.component.html'
})
export class MgSideNavRouterComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() routes: Routes;
  treeControl: FlatTreeControl<FileFlatNode>;
  treeFlattener: MatTreeFlattener<IFileNode, FileFlatNode>;
  dataSource: MatTreeFlatDataSource<IFileNode, FileFlatNode>;
  routeMap;
  @ViewChild('drawer') drawerRef: ElementRef;

  constructor(
    @Inject(FileDatabase) private database: FileDatabase,
    @Inject(SideNavControl) public sideNavControl: SideNavControl
  ) {

    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
    this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<FileFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit() {
    this.database.initialize(this.transformRoute(this.routes));
    this.database.dataChange.subscribe(data => this.dataSource.data = data);
  }

  ngAfterViewInit() {
    this.sideNavControl.attachDrawer(this.drawerRef);
  }

  ngOnDestroy() {
    this.sideNavControl.detachDrawer();
  }

  transformRoute(route: Routes) {
    return route.reduce((acc, cur, i) => {
      if (cur.children ) {
        acc[cur.path] = this.transformRoute(cur.children);
      } else {
        acc[cur.path] = cur.path;
      }
      return acc;
    }, {});
  }

  transformer = (node: Routes|any, level: number) => {
    return new FileFlatNode(!!node.children, node.filename, level, node.filename, node.url);
  }

  upperCaseFirstLevel(level: number, text: string) {
    return level === 0 ? text.toUpperCase(): this.jsUcfirst(text);
  }

  jsUcfirst(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  private _getLevel = (node: FileFlatNode) => node.level;

  private _isExpandable = (node: FileFlatNode) => node.expandable;

  private _getChildren = (node: IFileNode): Observable<IFileNode[]> => observableOf(node.children);

  hasChild = (_: number, _nodeData: FileFlatNode) => _nodeData.expandable;
}
