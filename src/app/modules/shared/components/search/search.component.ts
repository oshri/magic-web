import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'mg-search',
  styleUrls: ['./search.component.scss'],
  template: `<input #searchBox
    type="search"
    aria-label="search"
    placeholder="Search"
    (input)="doSearch()"
    (keyup)="doSearch()"
    (focus)="doFocus()"
    (click)="doSearch()">`
})
export class MgSearchComponent implements OnInit {

  private searchDebounce = 300;

  @ViewChild('searchBox') searchBox: ElementRef;


  ngOnInit() {

  }

  doSearch() {
    console.log('doSearch', this.query);
  }

  doFocus() {
    console.log('doFocus');
  }

  focus() {
    console.log('focus');
    this.searchBox.nativeElement.focus();
  }

  private get query() { return this.searchBox.nativeElement.value; }
  private set query(value: string) { this.searchBox.nativeElement.value = value; }
}
