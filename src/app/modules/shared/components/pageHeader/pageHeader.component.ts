import { Component, Input } from '@angular/core';

@Component({
  selector: 'mg-page-header',
  styleUrls: ['./pageHeader.component.scss'],
  templateUrl: './pageHeader.component.html'
})
export class MgPageHeaderComponent {
  @Input() title: string;
}
