// Modules
import { CommonModule } from '@angular/common';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  ModuleWithProviders,
  NgModule
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';

// Magic Components
import { MgSideNavRouterComponent } from './components/sideNavRouter/sideNavRouter.component';
import { MgDocPageComponent } from './components/docPage/docPage.component';
import { MgSearchComponent } from './components/search/search.component';
import { MgPageHeaderComponent } from './components/pageHeader/pageHeader.component';
import { MgCodeComponent } from './components/code/code.component';
import { MgCodeExampleComponent } from './components/codeExample/code-example.component';
import { MgCodeTabsComponent } from './components/codeTabs/code-tabs.component';

// Magic Services
import { MgSnackBar } from './services/snackBar/snackBar.service';
import { FileDatabase } from './services/fileFlatNode/fileFlatNode.service';
import { Logger } from './services/logger/logger.service';
import { CopierService } from './services/copier/copier.service';
import { PrettyPrinter } from './services/pretty/pretty-printer.service';

const COMMON_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  MaterialModule,
  RouterModule
];

const COMMON_COMPONENTS = [
  MgSideNavRouterComponent,
  MgDocPageComponent,
  MgSearchComponent,
  MgPageHeaderComponent,
  MgCodeComponent,
  MgCodeExampleComponent,
  MgCodeTabsComponent
];

const COMMON_ENTERY_COMPONENTS = [];

const COMMON_PROVIDERS = [
  MgSnackBar,
  FileDatabase,
  Logger,
  CopierService,
  PrettyPrinter
];

@NgModule({
  declarations: COMMON_COMPONENTS,
  exports: COMMON_COMPONENTS,
  imports: COMMON_MODULES,
  entryComponents: COMMON_ENTERY_COMPONENTS,
  providers: COMMON_PROVIDERS,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MgSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MgSharedModule
    };
  }
}
