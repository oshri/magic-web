import { NgModule } from '@angular/core';

import {
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatChipsModule,
  MatDialogModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatTabsModule,
  MatTableModule,
  MatSortModule,
  MatStepperModule,
  MatExpansionModule,
  MatSnackBarModule,
  MatTreeModule
} from '@angular/material';


@NgModule({
  imports: [
    MatAutocompleteModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatStepperModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatTreeModule
  ],
  exports: [
    MatAutocompleteModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatStepperModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatTreeModule
  ],
})
export class MaterialModule {}
