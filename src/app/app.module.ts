import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import 'hammerjs';


// Magic Main Routes
import { routes } from './routes';

// Magic Modules
import { MgCoreModule } from '@core/index';

// Magic Components
import { MgAppComponent } from './modules/core/containers/app/app.component';


const PROVIDERS: any = [

];


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    MgCoreModule.forRoot(),
  ],
  providers: [...PROVIDERS],
  bootstrap: [MgAppComponent]
})
export class AppModule {}
