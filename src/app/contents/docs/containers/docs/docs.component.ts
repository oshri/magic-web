import { Inject, Component, OnInit } from '@angular/core';
import { Routes, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'mg-docs',
  styleUrls: ['./docs.component.scss'],
  templateUrl: './docs.component.html'
})
export class MgDocsComponent implements OnInit {
  moduleRoutes: Routes;
  activeRoute$: Subscription;

  constructor(
    @Inject(ActivatedRoute) private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.moduleRoutes = this.route.routeConfig.children;
  }
}
