import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-item1',
  template: `
    <div>
      item1 works!

      <mg-code-example language="ts" linenums="2" class="special" title="Do Stuff">
        console.log('do stuff');
      </mg-code-example>
    </div>
  `,
  styles: []
})
export class Item1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
