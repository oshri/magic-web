import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-cheat-sheet-page',
  template: `
    <div>
      <h1>Cheat Sheet</h1>
      <div id="cheatsheet">
        <table class="is-full-width is-fixed-layout">
          <tbody><tr>
            <th>Magic Bootstrap Component</th>
            <th><p><code></code>
            </p>
            </th>
          </tr>
          <tr>
            <td>
              <code>
                magic-root
              </code>
            </td>
            <td><p>Bootstraps the app, using the root component from the specified <code><a href="api/core/NgModule" class="code-anchor">NgModule</a></code>. </p>
            </td>
          </tr>
          </tbody></table>
        <table class="is-full-width is-fixed-layout">
          <tbody><tr>
            <th>Magic Directive</th>
            <th><p><code></code>
            </p>
            </th>
          </tr>
          <tr>
            <td>
              <code>
                input type="text" magic="fieldId"
              </code>
            </td>
            <td><p>Defines a module that contains components, directives, pipes, and providers.</p>
            </td>
          </tr><tr>
            <td><code></code></td>
            <td><p>List of components, directives, and pipes that belong to this module.</p>
            </td>
          </tr><tr>
            <td><code></code></td>
            <td><p>List of modules to import into this module. Everything from the imported modules
              is available to <code></code> of this module.</p>
            </td>
          </tr>
          </tbody></table>        
      </div>
    </div>
  `,
  styles: []
})
export class CheatSheetPageComponent{

}
