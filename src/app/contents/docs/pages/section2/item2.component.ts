import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-item2',
  template: `
    <p>
      item2 works!
    </p>
  `,
  styles: []
})
export class Item2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
