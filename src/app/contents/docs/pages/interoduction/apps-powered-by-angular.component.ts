import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-apps-powered-by-angular',
  template: `
  <div>
  <h1>Introduction: Apps Powered by Angular</h1>
  <p>
    You can now easily create modern business apps powered by Angular to provide a rich user experience and meet the increasingly complex enterprise business expectations for digital transformation.
  </p>
  <p>
    Magic has always been known for thinking outside the box and now Magic has done away with the box. This means that you can now take your Magic xpa business logic for both; the back and the front end, and easily generate Angular code from it. The new Angular-based application can then work seamlessly with any Web presentation framework.
  </p>

  <h2>The new Web application framework gives you and your apps freedom in the following ways: </h2>
  <ul>
    <li>You’re free to tap into the world of Angular. </li>
    <li>Your apps are client-free since the deployed application is pure JS, HTML5, and CSS3.</li>
    <li>You can outsource the screens design.</li>
  </ul>
  <h2>Let’s break it down into four steps:</h2>

  <figure>
    <img src="assets/images/magicXpa/1.png" alt="Magic Studio" class="left" width="452" height="206">
  </figure>

  <ul>
    <li>In the Magic Studio, you define your business logic.</li>
    <li>In the Magic studio, with just a click of a button, you generate your Angular code using the Magic Converter, which is customizable by advanced users.</li>
    <li>In Angular, your code is now ready-to-use, but you can also add additional Angular code that is pure client-side logic using some Magic tags. </li>
    <li>Outside the Magic, you or a designer can update your screens using HTML and CSS.</li>
  </ul>
  <p>
    In this way, you can develop highly-responsive Web applications in no time. You can use the structured, low-code Magic xpa development platform for creating the business logic together with an open platform for the presenting your screens including the UI and the UX.
  </p>
  <br>
</div>

  `
})
export class AppsPoweredByAngularComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
