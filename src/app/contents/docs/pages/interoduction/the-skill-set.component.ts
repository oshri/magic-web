import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-the-skill-set',
  template: `
    <div>
      <h1>What You Need to Know: The Skill Set </h1>
      <p>These are the skills needed to create Web applications with the Web application framework:</p>
      <h3>Magic xpa</h3>
      <p>
        There’s a quick learning curve here, since the client is designed very much like usual Magic xpa Online and Rich Client programs. You develop the same way you are used to and use the new task type in Magic called Web Client.
      </p>
      <figure>
        <img src="assets/images/magicXpa/3.png" alt="Magic Studio" class="left" width="335" height="212">
      </figure>
      <h3> Angular</h3>
      <p>You, as a Magic developer, need to only know basic Angular. Once the Angular code is generated from Magic, the Angular part is over. Magic will generate three files: ts, html, and css.</p>
      <p>You will only need to understand the connectivity between Magic and Angular and how Magic’s engine is using Angular’s template syntax.  </p>
      <p>Angular’s goal is to let you extend the vocabulary of HTML, which you can do by defining your own tags. This ability to extend the vocabulary is how Magic syncs with Angular.</p>
      <figure>
        <img src="assets/images/magicXpa/4.png" alt="Magic Studio" class="left" width="275" height="152">
      </figure>
      <h3>Web Design</h3>
      <p>Once the Magic directives have been added to the Angular code, you will have functioning screens. </p>
      <p>Any external IDE can then be used to design the screens. Anyone who knows HTML and CSS can design the screens. About 90% of all the modifications needed will be to the HTML and CSS. Only about 10% work will be in Angular. </p>
      <br>
    </div>
  `,
  styles: []
})
export class TheSkillSetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
