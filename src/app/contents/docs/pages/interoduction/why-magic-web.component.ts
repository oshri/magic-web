import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-why-magic-web',
  template: `
  <div>
      <h1>Why Do You Need Magic’s Web Application Framework?</h1>
      <p>“Investment in web framework technologies today can improve existing systems and prepare your organization for the next
        generation of applications.” - Gartner Inc. </p>

      <h2>Magic and Angular Fit Well Together</h2>
      <p>It’s as if Magic had been waiting all of these years for a technology like Angular to come around so that Magic could best
        show off its development paradigm.</p>
      <p>By working with Magic to develop your Angular applications, you benefit from the best of both worlds since the two have
        a full synergetic fit. You define the business logic in Magic, both for the back end and for the front end, and develop
        the presentation using the Angular code. This synergy simplifies coding in Angular using the Magic technology and philosophy.
        It’s all about making life easier for the developer.</p>
      <p>Applications created with Magic’s Web application framework will be pure Angular applications, meaning they will be deployed
        and managed exactly like any Angular application.</p>

      <h2>State-of-the-art Technology</h2>
      <p>Magic’s Web application framework uses state-of-the-art client-side technologies including JavaScript, Angular, CSS3, HTML5,
        and TypeScript. You can create full-fledged business web-based applications that are heavy-duty, including data entry,
        transaction intensive, enabling you to integrate your applications with everything out there. </p>

      <h2>Magic xpa’s Client-side Architecture</h2>
      <p>All of the UI-related work, including the UI logic, is done by the browser (the client), which enables you to create fast
        and dynamic Web apps. In this architecture, the server only sends raw data to the client and the client generates the
        screen.</p>

      <p>The browser encompasses three layers:</p>
      <ul>
        <li>1. Angular coding, including HTML, CSS and TypeScript files. This is different for each application.</li>
        <li>2. Angular API. This is generic and the same for each application.</li>
        <li>3. Engine. This is also generic. </li>
      </ul>
      <p>A tag in the 1st area connects your Angular code to the Magic engine via the Angular API.</p>
      <p> PICTUE HERE !!!!!</p>

      <h2>Manageable Large Scale Angular Applications</h2>
      <p>Over the years, the number of lines of code needed to develop functionality-intensive Web apps, has been growing at a very
        rapid pace. </p>
      <p>As applications are evolving, the market keeps coming up with better ways to handle the ever-increasing lines of code.</p>
      <ul>
        <li>TypeScript was created to help manage the ever-growing number of JavaScripts lines in a big scale project (i.e. Visual
          Code).</li>
        <li>Angular was created to help manage complex Web-Apps that needs to provide great user experience.</li>
        <li>And now Magic’s Web application framework is coming along to help you better manage your Angular projects.</li>
      </ul>

      <p>PICTURE HERE !!!!</p>

      <p>Angular apps after a certain size are not manageable. For example, creating a 200 screen (web page) app in Angular is considered
        a large project – this is considered a small project when working with Magic's new Web client. With the new Web client,
        even creating a 2,000 screen enterprise SPA (Single-Page Application) app is easy. </p>

      <h2>Advanced Customization of Your UIs</h2>
      <p>The beauty of the decoupling of the UI and logic, which Magic’s Web application framework makes possible, is the flexibility
        to split the effort and time invested, between the business logic and the UI/UX.</p>
      <p>The two parts are equally important and time consuming, but when you have such an effective way of splitting the two you
        can reach much better results. The work can be done by the same developer (assuming the developer has the required skills)
        or by two different developers. For the UI/UX part, the developer does not require any Magic programing knowledge.</p>

      <h2>Less Code; More Versatility</h2>
      <p>The Magic engine’s robust business logic paradigm can be leveraged to save a lot of code in Angular by setting up most
        of the behind the scenes logic for the dynamic display of the presentation of the UI. </p>
      <p>Magic offers a wide range of control and form properties, which are very simple to use and can save a lot of lines of coding
        in Angular. For example, validation, recompute, conditions, visible, enabled, tooltips, and more.</p>
      <p>Expressions on any of the properties are very easy to use in Magic, which otherwise would take dozens of lines of codes
        to develop in Angular, especially when you are trying to achieve dynamic behavior in your apps. </p>
      <p>The appearance properties, such as Font, were removed and the basic appearance is done in HTML/CSS/Angular. The remaining
        properties are for either binding or behavior and these are set in Magic with no need to code these functionalities in
        Angular.</p>
      <p>So, by adding a tag to Angular for one of these binding or behavior functionalities, you are basically linking Magic to
        Angular. </p>
      <p>Here’s an example of how this works. Let’s say for example you want to work with a drop-down box. </p>
      <p>A drop-down box is made up of two main parts, the data and the option list, plus other properties. For the data, in Magic
        you just select the connected variable and for the options you set the Combo Box control’s Items List property. </p>
      <p>By doing this, your input control is now bound to the data-source field, and the control’s available options are now connected
        to your UI control. This saves you dozens of lines of code in Angular, where you would need to create a service for the
        options and have server-side validation for the input.  </p>

      <h2>Fully Compliant with Single-Page Applications (SPAs) </h2>
      <p>More and more organizations are selecting SPA methodology for their enterprise Web applications. Magic’s Web application
        framework is fully compliant with single-page applications (SPA). The SPAs provide a smoother and richer user experience.</p>
      <p>In an SPA solution, the first call to the server gets a single HTML page and from that point on, all HTML + CSS + JavaScript
        are created dynamically as the user interacts with the application to create fluid and responsive Web applications.</p>

        <p>PICTUE HERE !!!</p>

      <h2>Cloud Enabler: The Sky’s the Limit</h2>
      <p>Since the Web client is Web-enabled, running in the browser, the Web client is cloud enabled. This means that your apps
        can be deployed on any cloud. </p>

      <h2>Cross Browser Support and Portability</h2>
      <p>Your Web applications will be platform agnostic. They’ll run on any browser and on any device with zero footprint. You
        do not need to install anything.  </p>

      <h2>Ease of Maintenance in the Future</h2>
      <p>You can change your application logic from Magic and it will be reflected automatically in your Angular-based apps. Thus,
        there are less efforts to make changes in your application in future.</p>

      <h2>Full Git Support</h2>
      <p>Magic xpa is equipped to empower team development with its full Git support (including branching and merging).</p>
      <p>You can have two repositories located together in Git—one for Magic and one for Angular. This decouples the work of the
        two teams: one in charge of business logic and one in charge of the presentation.</p>

      <br>
  </div>

  `
})
export class WhyMagicWebComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
