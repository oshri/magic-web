import { Routes } from '@angular/router';
import { MgDocsComponent } from './containers/docs/docs.component';
import { MgDocPageComponent } from '@shared/components/docPage/docPage.component';
import {Item1Component} from './pages/section1/item1.component';
import {Item2Component} from './pages/section2/item2.component';
import {AppsPoweredByAngularComponent} from './pages/interoduction/apps-powered-by-angular.component';
import {TheSkillSetComponent} from './pages/interoduction/the-skill-set.component';
import {WhyMagicWebComponent} from './pages/interoduction/why-magic-web.component';
import {CheatSheetPageComponent} from './pages/fundamentals/cheat-sheet-page.component';

export const routes: Routes = [
  {
    path: '',
    component: MgDocsComponent,
    children: [
      {
        path : 'introduction',
        //component : MgDocPageComponent,
        children : [
          { path: 'apps powered by angular' , component : AppsPoweredByAngularComponent},
          { path: 'the skill set' , component : TheSkillSetComponent},
          { path: 'why magic web?' , component : WhyMagicWebComponent},

        ]
      },
      {
        path: 'fundamentals',
        //component: MgDocPageComponent,
        children: [
          {
            path: 'architecture',
            //component: MgDocPageComponent,
            children: [
              {
                path: 'architecture overview',
                component: MgDocPageComponent
              }
            ]
          },
          { path: 'cheat sheet' , component : CheatSheetPageComponent}
        ]
      },
      {
        path: 'section 1',
        children : [
          { path: 'item 1' , component : Item1Component },
          { path: 'item 2' , component : Item2Component },
          {
            path: 'section 2',
            children : [
              { path: 'item 1' , component : Item1Component },
              { path: 'item 2' , component : Item2Component }
            ]
          }
        ]
      }

    ]
  }
];
