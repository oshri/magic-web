import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '@material/index';
import { FlexLayoutModule } from '@angular/flex-layout';

// Magic Modules
import { routes } from './routes';

// Containers
import { MgDocsComponent } from './containers/docs/docs.component';
import {Item1Component} from './pages/section1/item1.component';
import {Item2Component} from './pages/section2/item2.component';
import {AppsPoweredByAngularComponent} from './pages/interoduction/apps-powered-by-angular.component';
import {MagicXpaPageComponent} from './pages/interoduction/magic-xpa-page.component';
import {TheSkillSetComponent} from './pages/interoduction/the-skill-set.component';
import {WhyMagicWebComponent} from './pages/interoduction/why-magic-web.component';
import {CheatSheetPageComponent} from './pages/fundamentals/cheat-sheet-page.component';
import {MgSharedModule} from '@shared/shared.module';



export const COMPONENTS = [
  MgDocsComponent,
  Item1Component,
  Item2Component,
  AppsPoweredByAngularComponent,
  MagicXpaPageComponent,
  TheSkillSetComponent,
  WhyMagicWebComponent,
  CheatSheetPageComponent,


];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MgSharedModule
    ],
    declarations: COMPONENTS,
    providers: [ ],
    entryComponents: []
})
export class DocsModule {
}
