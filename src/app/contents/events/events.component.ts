import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';

export interface EventElement {
  event: string;
  location: string;
  date: Date;
}

const ELEMENT_DATA: EventElement[] = [
  {event: 'ReactiveConf', location: 'Bratislava, Slovakia', date: new Date()},
  {event: 'AngularConnect', location: 'London, United Kingdom', date: new Date()},
  {event: 'ngAtlanta', location: 'Atlanta, Georgia	', date: new Date()},
  {event: 'ngVikings', location: 'Helsinki, Finland', date: new Date()},
  {event: 'ng-conf', location: 'Salt Lake City, UT', date: new Date()}
];

@Component({
  selector: 'mg-events',
  styleUrls: ['./events.component.scss'],
  templateUrl: './events.component.html'
})
export class MgEventsComponent implements OnInit {
  displayedColumns: string[] = ['event', 'location', 'date'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }
}
