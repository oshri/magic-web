import { Component } from '@angular/core';

@Component({
  selector: 'mg-resources',
  styleUrls: ['./resources.component.scss'],
  templateUrl: './resources.component.html'
})
export class MgResourcesComponent {}
