import { Inject, Component, OnInit } from '@angular/core';
import { Routes, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'mg-guides',
  styleUrls: ['./guides.component.scss'],
  templateUrl: './guides.component.html'
})
export class MgGuidesComponent implements OnInit {
  moduleRoutes: Routes;
  activeRoute$: Subscription;

  constructor(
    @Inject(ActivatedRoute) private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.moduleRoutes = this.route.routeConfig.children;
  }
}
