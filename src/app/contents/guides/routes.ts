import { Routes } from '@angular/router';
import { MgGuidesComponent } from './containers/guides/guides.component';
import { MgDocPageComponent } from '@shared/components/docPage/docPage.component';

export const routes: Routes = [
  {
    path: '',
    component: MgGuidesComponent,
    children: [
      {
        path: 'fundamentals',
        component: MgDocPageComponent,
        children: [
          {
            path: 'architecture',
            component: MgDocPageComponent,
            children: [
              {
                path: 'architecture overview',
                component: MgDocPageComponent
              }
            ]
          }
        ]
      }
    ]
  }
];
