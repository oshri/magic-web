import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '@material/index';
import { FlexLayoutModule } from '@angular/flex-layout';

// Magic Modules
import { routes } from './routes';

// Containers
import { MgGuidesComponent } from './containers/guides/guides.component';
import {MgSharedModule} from '@shared/shared.module';


export const COMPONENTS = [
  MgGuidesComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MgSharedModule
    ],
    declarations: COMPONENTS,
    providers: [ ],
    entryComponents: []
})
export class GuidesModule {
}
