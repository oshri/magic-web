import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes,
  query
} from '@angular/animations';

export const lightsAppearAnimation = [
    trigger('lightsAppear', [
        state('lightsOn', style({ opacity: 1 })),
        transition(':enter', [
            style({
                opacity: 0
            }),
            animate('1.5s ease-out')
        ]),
        transition(':leave', [
            style({
                opacity: 1
            }),
            animate(
                '0.3s ease-out',
                style({
                    opacity: 0
                })
            )
        ])
    ])
];

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* => *', [
    query(
      ':enter',
      [style({ opacity: 0 })],
      { optional: true }
    ),
    query(
      ':leave',
      [style({ opacity: 1 }), animate('0.3s', style({ opacity: 0 }))],
      { optional: true }
    ),
    query(
      ':enter',
      [style({ opacity: 0 }), animate('0.3s', style({ opacity: 1 }))],
      { optional: true }
    )
  ])
]);
